import math

from functools import total_ordering

infinity = float('inf')


class ConnectionNode:

    def __init__(self, first_port):
        self.connected_ports = {first_port}
        self.name = None

    def assimilate_node(self, node2):
        assert node2 is not self
        for port in node2.connected_ports:
            port.connection_node = self
        self.connected_ports.update(node2.connected_ports)
        node2.connected_ports.clear()

    def disconnect_port(self, port):
        self.connected_ports.discard(port)

    def __repr__(self):
        if self.name:
            return '<ConnectionNode {!r}>'.format(self.name)
        else:
            return '<ConnectionNode {!r}>'.format(self.connected_ports)


@total_ordering
class ConnectionPort:

    def __init__(self, component, port_name):
        self.component = component
        self.port_name = port_name
        self.connection_node = ConnectionNode(self)

    def __eq__(self, other):
        return self._data == other._data

    def __lt__(self, other):
        if self.component is other.component:
            return self.port_name < other.port_name
        else:
            return id(self.component) < id(other.component)

    def __hash__(self):
        return hash(self._data)

    @property
    def _data(self):
        return self.component, self.port_name

    def connect(self, *ports):
        for port in ports:
            if port in self.connection_node.connected_ports:
                continue
            self.connection_node.assimilate_node(port.connection_node)

    def connected_ports(self):
        return [port for port in self.connection_node.connected_ports
                if port != self]

    def disconnect(self):
        """Disconnect from all current connections"""
        self.connection_node.disconnect_port(self)
        self.connection_node = ConnectionNode(self)

    def __repr__(self):
        return '<Port {!r}:{!r}>'.format(self.component.name, self.port_name)


class Component:

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<{r} {!r}>'.format(type(self).__name__, self.name)

    def ports(self):
        raise NotImplementedError()


class VariableValue:

    def __init__(self, callback):
        self._callback = callback

    def get_value(self, global_data):
        return self._callback(global_data)


class Resistor(Component):

    def __init__(self, name, r):
        super().__init__(name)
        self.r = r
        self.port_plus = ConnectionPort(self, 'port_plus')
        self.port_minus = ConnectionPort(self, 'port_minus')

    def ports(self):
        return self.port_plus, self.port_minus

    def __repr__(self):
        return '<{!r} {!r} {!r} ohm>'.format(type(self).__name__,
                                             self.name, self.r)


class Switch(Resistor):

    def __init__(self, name, is_on_callback):
        r = VariableValue(lambda gd: 0 if is_on_callback(gd) else infinity)
        super().__init__(name, r)

    def __repr__(self):
        return '<{!r} {!r}>'.format(type(self).__name__, self.name)


class Inductor(Component):

    def __init__(self, name, l, initial_current=0):
        super().__init__(name)
        self.l = l
        self.initial_current = initial_current
        self.port_plus = ConnectionPort(self, 'port_plus')
        self.port_minus = ConnectionPort(self, 'port_minus')

    def ports(self):
        return self.port_plus, self.port_minus

    def __repr__(self):
        return '<{!r} {!r} {!r} H>'.format(type(self).__name__,
                                           self.name, self.l)


class Capacitor(Component):

    def __init__(self, name, c, initial_voltage=0):
        super().__init__(name)
        self.c = c
        self.initial_voltage = initial_voltage
        self.port_plus = ConnectionPort(self, 'port_plus')
        self.port_minus = ConnectionPort(self, 'port_minus')

    def ports(self):
        return self.port_plus, self.port_minus

    def __repr__(self):
        return '<{!r} {!r} {!r} F>'.format(type(self).__name__,
                                           self.name, self.c)


class VSource(Component):

    def __init__(self, name, v):
        super().__init__(name)
        self.v = v
        self.port_plus = ConnectionPort(self, 'port_plus')
        self.port_minus = ConnectionPort(self, 'port_minus')

    def ports(self):
        return self.port_plus, self.port_minus

    def __repr__(self):
        return '<{!r} {!r} {!r} V>'.format(type(self).__name__,
                                           self.name, self.v)


class ISource(Component):

    def __init__(self, name, i):
        super().__init__(name)
        self.i = i
        self.port_plus = ConnectionPort(self, 'port_plus')
        self.port_minus = ConnectionPort(self, 'port_minus')

    def ports(self):
        return self.port_plus, self.port_minus

    def __repr__(self):
        return '<{!r} {!r} {!r} A>'.format(type(self).__name__,
                                           self.name, self.i)


class Shape:

    class Sinus:

        def __init__(self, amplitude, frequency):
            self._amplitude = amplitude
            self._frequency = frequency

        def __call__(self, global_data):
            t = global_data.simulation_time
            return self._amplitude * math.sin(t * 2 * math.pi * self._frequency)


class VSignalGenerator(VSource):

    def __init__(self, name, voltage_callback):
        self._voltage_callback = voltage_callback
        v = VariableValue(self._voltage_callback)
        super().__init__(name, v)

    def __repr__(self):
        return '<{!r} {!r}>'.format(type(self).__name__, self.name)


class ISignalGenerator(ISource):

    def __init__(self, name, current_callback):
        self._current_callback = current_callback
        i = VariableValue(self._current_callback)
        super().__init__(name, i)

    def __repr__(self):
        return '<{!r} {!r}>'.format(type(self).__name__, self.name)


class IdealDiode(Component):
    """Perfect diode passes forward current with no resistance, blocks backward"""

    def __init__(self, name, threshold_voltage, forward_linear_resistance,
                 blocking_resistance=10**20):
        super().__init__(name)
        self.currently_open = False
        self.threshold_voltage = threshold_voltage
        self.forward_linear_resistance = forward_linear_resistance
        self.blocking_resistance = blocking_resistance
        self.port_plus = ConnectionPort(self, 'port_plus')
        self.port_minus = ConnectionPort(self, 'port_minus')

    def ports(self):
        return self.port_plus, self.port_minus

    def __repr__(self):
        return '<{!r} {!r}>'.format(type(self).__name__, self.name)


class FixedPotentialPoint(Component):

    def __init__(self, name, potential):
        super().__init__(name)
        self.potential = potential
        self.port = ConnectionPort(self, 'port')

    def ports(self):
        return self.port,

    def __repr__(self):
        return '<{!r} {!r}>'.format(type(self).__name__, self.name)


class Ground(FixedPotentialPoint):

    def __init__(self, name):
        super().__init__(name, 0)
