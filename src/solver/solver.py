from itertools import chain
import numpy as np

from components import Resistor, VSource, ISource, Inductor, Capacitor,\
    VariableValue, IdealDiode

from .topology import detect_nodes_branches_and_independent_loops,\
    circuit_sanity_check, transform_fixed_potential_points


class Solver:

    def __init__(self, first_port, skip_sanity_check=False):
        transform_fixed_potential_points(first_port)
        if not skip_sanity_check:
            circuit_sanity_check(first_port)

        self.nodes, self.branches, self.independent_loops, \
            self.branches_with_current_src, self.current_source_loops \
            = detect_nodes_branches_and_independent_loops(first_port)

        self.components = list(chain(*[branch.iter_components()
                                       for branch in self.branches]))

        self._check_consistency()

    def calculate_single_step(self, *result_variables):
        results = self.calculate(result_variables, 1, 2)
        assert len(results._results) == 2
        return results[1]

    def calculate(self, result_variables, time_step, duration):
        global_data = GlobalData()
        equations = self._create_equations(global_data, time_step)
        # self._print_debug_info()
        # equations._debug_print()

        has_diodes = any(isinstance(comp, IdealDiode)
                         for comp in self.components)

        times = np.arange(
            time_step, duration + time_step, time_step, dtype=np.double)
        for t in times:
            equations.update_inputs(global_data)
            # equations._debug_print()
            equations.solve()

            if has_diodes:
                wrong_diodes = []
                for comp in self.components:
                    if isinstance(comp, IdealDiode):
                        if comp.currently_open:
                            diode_i = equations.get_last_result(
                                'component current', comp)
                            if diode_i < 0:
                                wrong_diodes.append(comp)
                        else:
                            diode_v = equations.get_last_result(
                                'component voltage', comp)
                            if diode_v > comp.threshold_voltage:
                                wrong_diodes.append(comp)
                if wrong_diodes:
                    for diode in wrong_diodes:
                        diode.currently_open = not diode.currently_open
                    del equations._results[-1]
                    equations.update_inputs(global_data)
                    equations.solve()

            global_data.update(t)
        return equations.make_results(result_variables, times)

    def _create_equations(self, global_data, time_delta):

        # each branch has an associated current
        # each node has an associated voltage
        # each loop's sum of voltages is 0
        # each node's sum of currents is 0

        # There are B variables: current in each branch
        # there are L + N - 1 equations, which equals B

        # For each node:
        # I1 + I2 + I3 + ... = 0

        branches_without_current_source = [b for b in self.branches
                                           if b not in self.branches_with_current_src]

        output_variables = [('branch current', branch)
                            for branch in self.branches]
        output_variables.extend(('branch voltage', branch)
                                for branch in self.branches)
        output_variables.extend(('component current', component)
                                for component in self.components)
        output_variables.extend(('component voltage', component)
                                for component in self.components)

        # for i, var in enumerate(output_variables):
        #    print(i, var)
        #print('node equations:', len(self.nodes) - 1)
        #print('loop equations:', len(self.independent_loops))
        # print('branches with current sources equations:',
        #      len(self.branches_with_current_src))
        #print('component current equations:', len(self.components))
        #print('component voltage equations:', len(self.components))

        initial_values_dict = {}
        for comp in self.components:
            if isinstance(comp, ISource):
                initial_values_dict[('component current', comp)] = comp.i
            elif isinstance(comp, VSource):
                initial_values_dict[('component voltage', comp)] = comp.v
            if isinstance(comp, Inductor):
                initial_values_dict[
                    ('component current', comp)] = comp.initial_current
            elif isinstance(comp, Capacitor):
                initial_values_dict[
                    ('component voltage', comp)] = comp.initial_voltage

        def evaluate(value):
            if isinstance(value, VariableValue):
                return value.get_value(global_data)
            else:
                return value
        evaluated_initial_values_dict = {n: evaluate(v)
                                         for n, v in initial_values_dict.items()}
        equations = EquationSystem(output_variables,
                                   evaluated_initial_values_dict)

        # equations for sum of currents in a node = 0

        # skip the last node because it can be calculated from the others
        # (it contains no independent data)
        for i, node in enumerate(self.nodes[:-1], 1):
            eq = Equation(
                output_variables, name='node {} zero currents'.format(i))
            for branch in self.branches:
                if node in branch.nodes:
                    is_outward_current = (node == branch.nodes[0])
                    eq.set_factor('branch current', branch,
                                  1 if is_outward_current else -1)
            #print('>node eq')
            equations.add(eq)

        # equations for sum voltages in a loop = 0

        for i, loop in enumerate(self.independent_loops):
            eq = Equation(
                output_variables, name='loop {} zero voltage'.format(i))
            # sum of all voltages in the loop is 0
            for branch, is_forward in loop.iter_branches_and_directions():
                assert branch in branches_without_current_source
                for p1, p2 in branch.elements:
                    assert p1.component is p2.component
                    component = p1.component
                    assert p1.port_name in ('port_plus', 'port_minus')
                    if is_forward:
                        positive = p1.port_name == 'port_plus'
                    else:
                        positive = p1.port_name == 'port_minus'
                    eq.set_factor('component voltage', component,
                                  1 if positive else -1)
            #print('>loop eq')
            equations.add(eq)

        for i, branch in enumerate(self.branches, 1):
            # MAYBE TODO: skip loop-branches which always have zero voltage?
            eq = Equation(output_variables, name='branch {} voltage'.format(i))
            # -Vbranch + Vcomp1 + Vcomp2 - ... = 0
            eq.set_factor('branch voltage', branch, 1)
            for p1, p2 in branch.elements:
                assert p1.component is p2.component
                comp = p1.component
                assert p1.port_name in ('port_plus', 'port_minus')
                # Current goes backwards through sources (minus to plus)
                if p1.port_name == 'port_plus':
                    eq.set_factor('component voltage', comp, 1)
                else:
                    eq.set_factor('component voltage', comp, -1)
            #print('>branch voltage eq')
            equations.add(eq)
        # equations for I=const for branches with current sources

        for branch in self.branches_with_current_src:
            for p1, p2 in branch.elements:
                assert p1.component is p2.component
                comp = p1.component
                if isinstance(comp, ISource):
                    eq = Equation(
                        output_variables,
                        name='branch with ISource {} current'.format(comp.name))
                    assert p1.port_name in ('port_plus', 'port_minus')

                    # Current goes backwards through sources (minus to plus)
                    if p1.port_name == 'port_plus':
                        # Ibranch + Icomp = 0
                        eq.set_factor('branch current', branch, 1)
                        eq.set_factor('component current', comp, 1)
                    else:
                        # Ibranch - Icomp = 0
                        eq.set_factor('branch current', branch, 1)
                        eq.set_factor('component current', comp, -1)
                    equations.add(eq)

        for branch in self.branches:
            for p1, p2 in branch.elements:
                assert p1.component is p2.component
                assert p1.port_name in ('port_plus', 'port_minus')
                comp = p1.component
                if isinstance(comp, (ISource, VSource)):
                    positive = p1.port_name == 'port_minus'
                else:
                    positive = p1.port_name == 'port_plus'

                curr_eq = Equation(
                    output_variables, name='component {} current'.format(comp.name))

                if isinstance(comp, ISource):
                    # Isource - Icomp = 0
                    curr_eq.set_constant(comp.i)
                    curr_eq.set_factor('component current', comp, -1)
                else:
                    if positive:
                        # Icomp - Ibranch = 0
                        curr_eq.set_factor('component current', comp, 1)
                        curr_eq.set_factor('branch current', branch, -1)
                    else:
                        # Icomp + Ibranch = 0
                        curr_eq.set_factor('component current', comp, 1)
                        curr_eq.set_factor('branch current', branch, 1)
                #print('>component current eq')
                equations.add(curr_eq)

                volt_eq = Equation(
                    output_variables, name='component {} voltage'.format(comp.name))
                if isinstance(comp, Resistor):
                    # R * Icomp - Vcomp = 0
                    volt_eq.set_factor('component current', comp, comp.r)
                    volt_eq.set_factor('component voltage', comp, -1)
                elif isinstance(comp, VSource):
                    # Vsrc - Vcomp = 0
                    volt_eq.set_factor('component voltage', comp, -1)
                    volt_eq.set_constant(comp.v)
                elif isinstance(comp, ISource):
                    # There is no voltage calculation available for the
                    # current source component. Instead of it, there has to be
                    # an equation which calculates the branch voltage from
                    # other voltages. Source voltage will in the end be
                    # calculated from that.
                    loop = self.current_source_loops[branch]
                    loop_branches = list(loop.iter_branches_and_directions())
                    branch0, is_forward0 = loop_branches[0]
                    assert branch0 is branch and is_forward0
                    volt_eq.set_factor('branch voltage', branch, -1)
                    for b, is_forward in loop_branches[1:]:
                        volt_eq.set_factor(
                            'branch voltage', b, 1 if is_forward else -1)
                elif isinstance(comp, Inductor):
                    # Vcomp = L * dIcomp/dT
                    #      ~= L * deltaIcomp/deltaT
                    # Vcomp = L * (Icomp / deltaT) - L * (I0 / deltaT)
                    # Vcomp - (L / deltaT) * Icomp + (L * I0 / deltaT) = 0
                    volt_eq.set_factor('component voltage', comp, 1)
                    volt_eq.set_factor(
                        'component current', comp, -comp.l / time_delta)

                    def inductor_voltage(global_data, volt_eq=volt_eq,
                                         comp=comp, time_delta=time_delta):
                        i0 = volt_eq.get_previous_state(
                            'component current', comp)
                        return i0 * comp.l / time_delta

                    volt_eq.set_constant_callback(inductor_voltage)
                elif isinstance(comp, Capacitor):
                    # Icomp = C * dVcomp / dT
                    #      ~= C * deltaVcomp / deltaT
                    # Icomp - (C / deltaT) * Vcomp + (V0 * C / deltaT)
                    volt_eq.set_factor('component current', comp, 1)
                    volt_eq.set_factor(
                        'component voltage', comp, -comp.c / time_delta)

                    def capacitor_current(global_data, volt_eq=volt_eq,
                                          comp=comp, time_delta=time_delta):
                        v0 = volt_eq.get_previous_state(
                            'component voltage', comp)
                        return v0 * comp.c / time_delta

                    volt_eq.set_constant_callback(capacitor_current)
                elif isinstance(comp, IdealDiode):
                    # two steps:
                    # first: assume current state (open or closed)
                    # then: verify v<=0 if closed; i>=0 if opened
                    # if verification fails: calculate with opposite state

                    #==========================================================
                    # if comp.currently_open:
                    # --- V = threshold_voltage + I * forward_linear_resistance
                    #     volt_eq.set_constant(threshold_voltage)
                    #     volt_eq.set_factor('component voltage', comp, -1)
                    #     volt_eq.set_factor('component current', comp, forward_linear_resistance)
                    # else:
                    # --- V = I * blocking_resistance
                    #     volt_eq.set_constant(0)
                    #     volt_eq.set_factor('component voltage', comp, -1)
                    #     volt_eq.set_factor('component current', comp, blocking_resistance)
                    #==========================================================

                    volt_eq.set_constant_callback(lambda gd, comp=comp:
                                                  comp.threshold_voltage
                                                  if comp.currently_open else 0)
                    volt_eq.set_factor('component voltage', comp, -1)
                    volt_eq.set_factor_callback('component current', comp,
                                                lambda gd, comp=comp:
                                                comp.forward_linear_resistance
                                                if comp.currently_open
                                                else comp.blocking_resistance)
                else:
                    raise Exception(
                        'Not implemented for {}'.format(type(comp)))
                #print('>component voltage eq')
                equations.add(volt_eq)

        return equations

    def _check_consistency(self):
        """Mathematical consistency check"""
        assert len(self.nodes) + len(self.independent_loops) + \
            len(self.branches_with_current_src) == len(self.branches) + 1
        assert len(self.branches) == len(set(self.branches))
        assert len(self.branches_with_current_src) == len(
            set(self.branches_with_current_src))
        assert set(self.branches).issuperset(self.branches_with_current_src)

        for loop in self.independent_loops:
            for branch in loop.branches:
                assert branch in self.branches
                assert branch not in self.branches_with_current_src
                for node in branch.nodes:
                    assert node in self.nodes

    def _print_debug_info(self):
        nodes, branches, independent_loops, branches_with_current_src, components = \
            self.nodes, self.branches, self.independent_loops, \
            self.branches_with_current_src, self.components

        print('{}N {}B {}L {}C'.format(len(nodes), len(branches), len(independent_loops),
                                       len(branches_with_current_src)))
        for i, node in enumerate(nodes):
            print('node {}: {}'.format(i, node))
        for i, branch in enumerate(branches):
            print('branch {}: {}'.format(i, branch))
        for i, loop in enumerate(independent_loops):
            print('loop {}:'.format(i))
            loop_node = None
            branch0 = None
            for branch, is_forward in loop.iter_branches_and_directions():
                if branch0 is None:
                    branch0 = branch
                    is_forward0 = is_forward
                    loop_node = branch.nodes[1 if is_forward else 0]
                else:
                    if is_forward:
                        assert loop_node is branch.nodes[0]
                        loop_node = branch.nodes[1]
                    else:
                        assert loop_node is branch.nodes[1]
                        loop_node = branch.nodes[0]
                print(
                    '       - {} {}'.format('FWD' if is_forward else 'BACK', branch))
            assert loop_node is branch0.nodes[0 if is_forward0 else 1]
        for i, branch in enumerate(branches_with_current_src):
            print('current source branch {}: {}'.format(i, branch))
        for i, comp in enumerate(components):
            print('comp {}: {}'.format(i, comp))
        print()


class GlobalData:

    def __init__(self):
        self.simulation_time = 0

    def update(self, simulation_time):
        self.simulation_time = simulation_time


class Results:

    def __init__(self, result_variables, input_array, output_variables, results):
        self._result_variables = result_variables
        self.input_array = input_array
        self._output_variables = list(output_variables)
        self._output_variable_keys = tuple((var_type, id(obj))
                                           for var_type, obj
                                           in output_variables)
        self._indices = {
            key: i for i, key in enumerate(self._output_variable_keys)}
        self._results = results

    def get_input_array(self):
        return self.input_array

    def get_results(self, key_type, key_object):
        if (key_type, key_object) not in self._result_variables:
            raise ValueError("Results for {}:{} were not requested"
                             .format(key_type, key_object))
        index = self._indices[(key_type, id(key_object))]
        return self._results[..., index]

    def __len__(self):
        return len(self._results)

    def __getitem__(self, i):
        assert isinstance(i, int)
        return SingleResult(self._output_variables, self._results[i])


class SingleResult:

    def __init__(self, output_variables, result):
        self._output_variables = list(output_variables)
        self._output_variable_keys = tuple((var_type, id(obj))
                                           for var_type, obj
                                           in output_variables)
        self._indices = {
            key: i for i, key in enumerate(self._output_variable_keys)}
        self._result = result

    def get_result(self, key_type, key_object):
        index = self._indices[(key_type, id(key_object))]
        return self._result[index]

    def __repr__(self):
        return '<SingleResult {!r} {!r}>'.format(self._result, self._output_variables)

    def print_results(self):
        for value, name in zip(self._result, self._output_variables):
            print(value, name)


class EquationSystem:

    def __init__(self, output_variables, initial_values_dict):
        self._output_variables = list(output_variables)
        self._output_variable_keys = tuple((var_type, id(obj))
                                           for var_type, obj
                                           in output_variables)
        self._indices = {
            key: i for i, key in enumerate(self._output_variable_keys)}
        self._equations = []
        initial_values_array = [initial_values_dict.get(k, 0)
                                for k in output_variables]
        self._results = [np.array(initial_values_array, np.double)]
        assert self._results[0].shape == (len(self._output_variables), )

    def make_results(self, result_variables, input_array):
        results_matrix = np.array(self._results[1:], np.double)
        return Results(result_variables, input_array, self._output_variables, results_matrix)

    def add(self, eq):
        assert eq._equation_system is None
        self._equations.append(eq)
        eq._equation_system = self
        assert eq._equation_system is self
        self._eqs_with_updates = None

    def update_inputs(self, global_data):
        eqs = self._eqs_with_updates
        if eqs is None:
            eqs = [eq for eq in self._equations if eq.has_dynamic_data()]
            self._eqs_with_updates = eqs
        for eq in eqs:
            eq.update_eq_values(global_data)

    def solve(self):
        rank = len(self._output_variables)
        assert rank == len(self._equations), \
            "Cannot calculate {} variables from {} equations".format(
            rank, len(self._equations))
        assert all(rank == eq.rank for eq in self._equations), \
            'Bad rank: {}'.format([eq.rank for eq in self._equations])
        a = np.array([eq._factors for eq in self._equations], np.double)
        b = -np.array([eq._constant for eq in self._equations], np.double)
        try:
            results = np.linalg.solve(a, b)
        except np.linalg.linalg.LinAlgError as e:
            raise EquationsNotSolvable() from e

        self._results.append(results)

    def get_last_result(self, key_type, key_object):
        index = self._indices[(key_type, id(key_object))]
        return self._results[-1][index]

    def _debug_print(self):
        def typestr(t):
            return {'branch current': 'IB',
                    'branch voltage': 'VB',
                    'component current': 'IC',
                    'component voltage': 'VC',
                    }[t]

        def obj_str(o):
            from .topology import Branch
            from components import Component
            if isinstance(o, Branch):
                branches = [obj for t, obj in self._output_variables
                            if isinstance(obj, Branch)]
                return 'b' + str(branches.index(o) + 1)
            elif isinstance(o, Component):
                return o.name

        def signed(number):
            if number >= 0:
                return '+ {}'.format(number)
            else:
                return '- {}'.format(-number)

        def factored(factor, var):
            if factor == 0:
                return ''
            elif factor == 1:
                return ' + {}'.format(var)
            elif factor == -1:
                return ' - {}'.format(var)
            else:
                return '{} x {}'.format(signed(factor), var)
        print(100 * '-')
        for i, eq in enumerate(self._equations, 1):
            factor_strings = (factored(f, '{}_{}'.format(typestr(key_type), obj_str(key_obj)))
                              for f, (key_type, key_obj) in zip(eq._factors, self._output_variables)
                              if f)
            vars_str = ' '.join(factor_strings)
            if eq._constant:
                constant_str = ' {}'.format(signed(eq._constant))
            else:
                constant_str = ''

            eqstr = '{}: {}{} = 0'.format(i, vars_str, constant_str)
            if eq.name:
                eqstr = '%-70s   # %s' % (eqstr, eq.name)
            print(eqstr)
        print(100 * '-')


class EquationsNotSolvable(Exception):
    pass


class Equation:
    """Representation of F1 * X1 + F2 * X2 + ... + Fn * Xn + C = 0

    F1, F2, F3, ... are self.factors
    C is self.constant
    n is self.rank
    """

    def __init__(self, output_variables, name=None):
        self.name = name
        self._output_variables = output_variables
        self._output_variable_keys = tuple((var_type, id(obj))
                                           for var_type, obj
                                           in output_variables)
        self._indices = {
            key: i for i, key in enumerate(self._output_variable_keys)}
        rank = len(self._output_variable_keys)
        self._factors = np.array(rank * [0], np.double)
        self._constant = 0
        self._constant_callback = None
        self._factor_callbacks = {}
        self._equation_system = None

    def has_dynamic_data(self):
        return self._constant_callback or self._factor_callbacks

    def update_eq_values(self, global_data):
        assert self._equation_system
        if self._constant_callback:
            self._constant = self._constant_callback(global_data)
        for index, callback in self._factor_callbacks.items():
            self._factors[index] = callback(global_data)

    def get_previous_state(self, key_type, key_object):
        assert self._equation_system
        return self._equation_system.get_last_result(key_type, key_object)

    @property
    def rank(self):
        assert self._equation_system
        return len(self._factors)

    def set_factor_callback(self, key_type, key_object, callback):
        """Callback receives one argument: global_data

        If other other data is needed, callback should have references to them
        itself.
        """
        index = self._indices[(key_type, id(key_object))]
        self._factor_callbacks[index] = callback

    def set_factor(self, key_type, key_object, factor):
        if isinstance(factor, VariableValue):
            self.set_factor_callback(key_type, key_object, factor.get_value)
        else:
            index = self._indices[(key_type, id(key_object))]
            self._factors[index] = factor

    def set_constant_callback(self, callback):
        """Callback receives one argument: global_data

        If other other data is needed, callback should have references to them
        itself.
        """
        self._constant_callback = callback

    def set_constant(self, constant):
        if isinstance(constant, VariableValue):
            self.set_constant_callback(constant.get_value)
        else:
            self._constant = constant
