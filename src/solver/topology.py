"""Topological analysis of an electrical circuit.

Detects all relevant inforamtion about connected components, which will be
needed for the calculations.

This is not used for defining a circuit, nor for solving/simulating it.
It is meant just an intermediate step before solving the circuit.

"""
from components import ISource, Switch, VSource, FixedPotentialPoint


def transform_fixed_potential_points(first_port):
    """Replace GND, +5V +3.3V etc. components with voltage sources

    If there is only one such point, it is just removed, but if there are more
    with different voltages, voltage sources are created between them.

    This is done IN PLACE, i.e. the input circuit is modified.

    """
    all_nodes_p = [list(n.connected_ports)[0]
                   for n in _iter_nodes(first_port.connection_node)]
    all_components = set(port.component
                         for node_p in all_nodes_p
                         for port in node_p.connection_node.connected_ports)
    fixed_potential_points = [comp for comp in all_components
                              if isinstance(comp, FixedPotentialPoint)]

    if not fixed_potential_points:
        return

    all_potentials = set(comp.potential for comp in fixed_potential_points)
    points_by_potential = {potential: [comp for comp in fixed_potential_points
                                       if comp.potential == potential]
                           for potential in all_potentials}
    first_potential = next(iter(all_potentials))
    first_comp = points_by_potential[first_potential][0]
    for potential, comps in points_by_potential.items():
        if potential != first_potential:
            vsource = VSource(
                'Generated VSource', potential - first_potential)
            for comp in comps:
                vsource.port_plus.connect(comp.port)
                vsource.port_minus.connect(first_comp.port)
                comp.port.disconnect()
    for comp in points_by_potential[first_potential]:
        if comp is not first_comp:
            comp.port.connect(first_comp.port)
            comp.port.disconnect()
    first_comp.port.disconnect()


def circuit_sanity_check(first_port):
    """Verify that the circuit is solvable at all or raise a SanityCheckFailed

    An "unsolvable" circuit is one with mathematical impossibility,
    e.g. one with two current sources connected in series, or a short-circuited
    voltage source etc.

    There are no unsolvable circuits in real life, but that is because there
    are no ideal components in real life - there are always some internal
    resistances, capacities etc. which would make the circuits solvable
    in the mathematical model as well.

    Therefore, a modification can always be applied to a circuit to make it
    solvable.

    Circuits which are "difficult" to solve because of e.g. non-linear
    components are not "unsolvable". They must pass this check.

    """
    nodes, branches = detect_nodes_and_branches(first_port)

    branches_with_current_sources = [
        branch for branch in branches
        if any(isinstance(comp, ISource)
               for comp in branch.iter_components())]

    branches_without_current_sources_or_switches = [
        branch for branch in branches
        if not any(isinstance(comp, (ISource, Switch))
                   for comp in branch.iter_components())]

    branches_without_isource_or_switch_per_node = {
        node: [branch for branch in branches_without_current_sources_or_switches
               if node in branch.nodes]
        for node in nodes}

    branches_per_node = {
        node: [branch for branch in branches if node in branch.nodes]
        for node in nodes}

    for branch in branches:
        current_sources = [comp for comp in branch.iter_components()
                           if isinstance(comp, ISource)]
        switches = [comp for comp in branch.iter_components()
                    if isinstance(comp, Switch)]

        if len(current_sources) > 1:
            raise SanityCheckFailed(
                "Serially connected current sources are impossible to solve.",
                current_sources)

        if len(switches) > 1:
            raise SanityCheckFailed(
                "Serially connected switches are not solvable (unknown voltages).",
                switches)

        if len(current_sources) + len(switches) > 1:
            raise SanityCheckFailed(
                "Current sources serially connected to switches are unsolvable.",
                current_sources + switches)

        if len(current_sources) + len(switches) > 1:
            raise SanityCheckFailed(
                "Current sources serially connected to switches are unsolvable.",
                current_sources + switches)

    for branch in branches_with_current_sources:
        try:
            _find_loop(branch, branches_without_isource_or_switch_per_node,
                       set())
        except _NoLoopFound:
            isource, = [comp for comp in branch.iter_components()
                        if isinstance(comp, ISource)]
            raise SanityCheckFailed(
                "There is no available circuit for the current from the "
                "current source to flow.",
                [isource])

    switch_branches = [b for b in branches
                       if any(isinstance(comp, Switch)
                              for comp in b.iter_components())]

    for switch_branch in switch_branches:
        if switch_branch.nodes[0] is switch_branch.nodes[1]:
            # not a dangling branch
            continue
        if any(len(branches_per_node[node]) < 2
               for node in switch_branch.nodes):
            switches = [comp for comp in switch_branch.iter_components()
                        if isinstance(comp, Switch)]
            raise SanityCheckFailed(
                "Voltage of a switch in a dangling branch is not solvable.",
                switches)

    vsource_switch_branches = [b for b in branches
                               if all(isinstance(comp, (VSource, Switch))
                                      for comp in b.iter_components())]

    vsource_branches = [b for b in vsource_switch_branches
                        if any(isinstance(comp, VSource)
                               for comp in b.iter_components())]

    self_looped_vsource_branches = [b for b in vsource_branches
                                    if b.nodes[0] is b.nodes[1]]

    for self_looped_vsource_branch in self_looped_vsource_branches:
        raise SanityCheckFailed(
            "Self-looped voltage source is not solvable.",
            list(self_looped_vsource_branch.iter_components()))

    branches_with_only_vsource_or_switch_per_node = {
        node: [branch for branch in vsource_switch_branches
               if node in branch.nodes]
        for node in nodes}

    for branch in vsource_branches:
        try:
            _find_loop(branch, branches_with_only_vsource_or_switch_per_node,
                       set())
        except _NoLoopFound:
            pass  # OK
        else:
            vsources = [comp for comp in branch.iter_components()
                        if isinstance(comp, VSource)]
            raise SanityCheckFailed(
                "Ideal voltage source in conflict with the topology",
                vsources)

    branches_without_switches_per_node = {
        node: [branch for branch in branches
               if not any(isinstance(comp, Switch)
                          for comp in branch.iter_components())
               if node in branch.nodes]
        for node in nodes}

    for branch in switch_branches:
        try:
            _find_loop(branch, branches_without_switches_per_node, set())
        except _NoLoopFound:
            switches = [comp for comp in branch.iter_components()
                        if isinstance(comp, Switch)]
            raise SanityCheckFailed(
                "Switch voltage cannot always be determined if OFF",
                switches)

    for parallel_branches in _get_parallel_branches(switch_branches).values():
        if len(parallel_branches) > 1:
            raise SanityCheckFailed(
                "Switches connected in parallel are not solvable (unknown current).",
                [comp for branch in parallel_branches
                 for comp in branch.iter_components()])


class SanityCheckFailed(Exception):

    def __init__(self, msg, components):
        self.components = tuple(components)
        msg2 = "{}\nComponents: {!r}".format(msg, self.components)
        super().__init__(msg2)


def _get_parallel_branches(branches):
    """Get branches which have the same nodes on their ends

    Return a dictionary of {frozenset(node1,node2): [branches]}
    """
    parallel_branches = {}
    for branch in branches:
        nodes = frozenset(branch.nodes)
        parallel_branches.setdefault(nodes, [])
        parallel_branches[nodes].append(branch)
    return parallel_branches


def detect_nodes_and_branches(first_port):
    """"Get list of ConnectionNode and list of Branch objects"""

    all_nodes = list(_iter_nodes(first_port.connection_node))
    all_branching_nodes = [n for n in all_nodes
                           if len(n.connected_ports) > 2
                           or len(n.connected_ports) == 1]
    branches = []
    if not all_branching_nodes:
        # There are no branching nodes. Pick any. It is circular.
        node = first_port.connection_node
        branches = list(_iter_branches(node))
        assert len(branches) == 1
        branch = branches[0]
        assert node is branch.nodes[0] and node is branch.nodes[1]
        all_branching_nodes = [branch.nodes[0]]
    else:
        processed_nodes = set()
        for node in all_branching_nodes:
            for branch in _iter_branches(node):
                assert branch.nodes[0] is node
                if branch.nodes[1] in processed_nodes:
                    continue
                branches.append(branch)
            processed_nodes.add(node)

    return all_branching_nodes, branches


class Branch:
    """A part of a circuit with only serially connected components

    Branches and nodes are basic elements of a circit topology analysis.
    Nodes are points where branches begin and end. A branch always has two
    nodes. A node may be conencted to one branch, or to three or more branches,
    but not zero and not two. A branch may contain multiple components, but it
    may have connections to other branches only in its nodes. Current flowing
    through a brach is the same throughout the same branch.
    """

    def __init__(self, node1, node2, branch_elements):
        """Intermediate object in topological analysis

        - nodes are ConnectionNode objects
        - branch_elements are ConnectionPort pairs

        """
        self.nodes = (node1, node2)
        self.elements = tuple(branch_elements)

    def reversed(self):
        elems = tuple((p2, p1) for p1, p2 in reversed(self.elements))
        n1, n2 = self.nodes
        return Branch(n2, n1, elems)

    def other_node(self, node):
        assert node in self.nodes
        return self.nodes[1] if node == self.nodes[0] else self.nodes[0]

    def iter_components(self):
        for p1, p2 in self.elements:
            assert p1.component is p2.component
            yield p1.component

    def __eq__(self, other):
        return self._data == other._data

    def __hash__(self):
        return hash(self._data)

    def __repr__(self):
        return '<Branch {1!r} {0!r}>'.format(self.nodes, self.elements)

    @property
    def _data(self):
        return self.nodes, self.elements


class Loop:
    """A list of serially connected branches looping back to the same node."""

    def __init__(self, branches):
        self.branches_with_directions = []
        last_node = None
        for branch in branches:
            if not self.branches_with_directions:
                n1, n2 = branch.nodes
                self.branches_with_directions.append((branch, True))
                last_node = n2
            else:
                assert last_node is not None
                if last_node not in branch.nodes:
                    if len(self.branches_with_directions) == 1:
                        (b0, fwd0), = self.branches_with_directions
                        fwd0 = not fwd0
                        self.branches_with_directions[:] = [(b0, fwd0)]
                        last_node = b0.nodes[1 if fwd0 else 0]

                assert last_node in branch.nodes, \
                    "Bad loop path {}".format(branches)
                n1, n2 = branch.nodes
                if last_node is n1:
                    self.branches_with_directions.append((branch, True))
                    last_node = n2
                else:
                    assert last_node is n2
                    self.branches_with_directions.append((branch, False))
                    last_node = n1
        branch0, is_fwd0 = self.branches_with_directions[0]
        assert last_node is branch0.nodes[0 if is_fwd0 else 1]

    @property
    def branches(self):
        return tuple(branch for branch, _is_forward
                     in self.branches_with_directions)

    def iter_branches_and_directions(self):
        """Yield (branch, is_forward) tuples"""
        last_node = None
        for branch, is_forward in self.branches_with_directions:
            n1 = branch.nodes[0 if is_forward else 1]
            n2 = branch.nodes[1 if is_forward else 0]
            if last_node is not None:
                assert last_node is n1
                last_node = n2
            yield branch, is_forward


def detect_nodes_branches_and_independent_loops(first_port):
    """The heart of the circuit analysis. Finds all interesting information

    Returns a tuple of:
    - nodes: list of all nodes in the circuit
    - branches: list of all branches in the circuit
    - independent_loops: list of all independent loops
                         ("independent" in the meaning needed for solving the
                          system of linear equations for solving the circuit;
                          this does not include branches with current sources)
    - branches_with_current_sources: list of branches which do not contain
                        current sources
    - current_source_loops: dict of loops, each containing one branch with a
                        current source
    """
    nodes, branches = detect_nodes_and_branches(first_port)

    branches_without_current_sources = [
        b for b in branches
        if not any(isinstance(comp, ISource)
                   for comp in b.iter_components())]

    branches_with_current_sources = [
        b for b in branches
        if any(isinstance(comp, ISource)
               for comp in b.iter_components())]

    branches_per_node = {node: [branch for branch in branches_without_current_sources
                                if node in branch.nodes]
                         for node in nodes}

    number_of_independent_loops = len(branches_without_current_sources) \
        - len(nodes) + 1

    used_branches = set()

    independent_loops = []
    if branches_without_current_sources:
        first_branch = branches_without_current_sources[0]
        for _i in range(number_of_independent_loops):
            loop = _find_loop(first_branch, branches_per_node, used_branches)
            independent_loops.append(loop)
            used_branches.update(loop.branches)

    current_source_loops = {}
    for branch in branches_with_current_sources:
        current_source_loops[branch] = _find_loop(
            branch, branches_per_node, set())

    return nodes, branches, independent_loops, branches_with_current_sources,\
        current_source_loops


def _iter_paths(node, branches_per_node, seen_nodes, path_so_far):
    """Follow connected branches and yield them as paths, width first.

    Does not enter the same node twice (does not form loops)

    node: the first node to start iterating
    branches_per_node: circuit definition as a {node: [branches]} dictionary
    seen_nodes: nodes not to be entered (needed for recursion)
    path_so_far: used as prefix for the result values (needed for recursion)

    Yields (last_node, [path_of_branches]) tuples.
    """
    node_branches = branches_per_node[node]
    next_nodes = []
    next_seen_nodes = seen_nodes | {node}
    for branch in node_branches:
        next_node = branch.other_node(node)
        if next_node in seen_nodes:
            continue
        if branch in path_so_far:
            continue
        path = path_so_far + [branch]
        yield next_node, path
        next_nodes.append((next_node, path))

    for next_node, path in next_nodes:
        for deeper_path in _iter_paths(next_node,
                                       branches_per_node,
                                       next_seen_nodes,
                                       path):
            yield deeper_path


def _find_loop(starting_branch, branches_per_node, used_branches):
    """Find a Loop which includes starting_branch and at least one unused branch.

    This will find the shortest loop which includes the starting branch and
    contains at least one branch which is not in the used_branches.

    branches_per_node defines the circuit by specifying all branches connected
    to each node, in a {node: [branches]} dictionary.
    """
    node1, node2 = starting_branch.nodes
    if node1 == node2:
        # The branch is a loop all by itself
        return Loop([starting_branch])

    for node, path in _iter_paths(node2, branches_per_node,
                                  set(), [starting_branch]):
        if node == node1:
            if used_branches.issuperset(path):
                continue
            return Loop(path)
    raise _NoLoopFound('Could not find another loop')


class _NoLoopFound(Exception):
    pass


def _iter_branches(node):
    """Find all branches starting from the node

    Yield Branch objects

    A branch is considered to be a stream of components which are linearily
    connected without any connection branching.
    """

    # Handling special case of branches which start and end in the same node:
    # - this would "normally" be caounted twice because they are encountered
    #   in both directions
    # - remember them and check for duplicates later
    encountered_circular_branches = set()

    for port in node.connected_ports:
        branch_elements = []

        port1 = port
        while True:
            component = port1.component
            component_ports = list(component.ports())
            assert len(component_ports) == 2
            assert port1 in component_ports
            port2 = component_ports[0] \
                if port1 == component_ports[1] else component_ports[1]
            assert port1 != port2
            branch_elements.append((port1, port2))
            next_node = port2.connection_node
            next_node_ports = list(next_node.connected_ports)
            assert len(next_node_ports) >= 1
            if len(next_node_ports) != 2:
                # 1 connection in node: a dangling branch
                # >2 connections in node: a "normal" branch
                # 2 connections in node: middle of a branch, not an end at all
                yield Branch(node, next_node, branch_elements)
                break
            elif next_node == node:
                # circular branch back to the same node
                branch = Branch(node, next_node, branch_elements)
                if branch.reversed() in encountered_circular_branches:
                    break
                yield branch
                encountered_circular_branches.add(branch)
                break
            else:
                assert port2 in next_node_ports
                port1 = next_node_ports[0] \
                    if next_node_ports[1] == port2 else next_node_ports[1]


def _iter_nodes(first_node, seen_nodes=None):
    """Find all nodes in the circuit, starting from one node"""

    if seen_nodes is None:
        seen_nodes = set()
    if first_node in seen_nodes:
        return
    seen_nodes.add(first_node)
    yield first_node
    for port in first_node.connected_ports:
        for other_port in port.component.ports():
            other_node = other_port.connection_node
            for x in _iter_nodes(other_node, seen_nodes):
                yield x
