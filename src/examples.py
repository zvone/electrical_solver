
import time
import math
import matplotlib.pyplot as plt

from components import Resistor, VSource, ISource, Inductor, Capacitor, Switch,\
    IdealDiode, Shape, VSignalGenerator
from solver.solver import Solver


def example_circuit1():
    """
        --------------------------
        |+       |        |+     |+
        Rp       |        RL     R1
        |        |+       |      |
        |+       I1       |+     |+
        E1       |        L1     R2
        |        |        |      |
        -------------------------

    """
    R1 = Resistor('R1', 1)
    R2 = Resistor('R2', 2)
    Rp = Resistor('Rp', 3)
    E1 = VSource('E1', 3)
    I1 = ISource('I1', 6)
    RL = Resistor('RL', 3)
    L1 = Inductor('L1', 0.2)

    I1.port_plus.connect(R1.port_plus)
    R1.port_minus.connect(R2.port_plus)
    R2.port_minus.connect(I1.port_minus)
    Rp.port_plus.connect(I1.port_plus)
    Rp.port_minus.connect(E1.port_plus)
    E1.port_minus.connect(I1.port_minus)
    RL.port_plus.connect(I1.port_plus)
    RL.port_minus.connect(L1.port_plus)
    L1.port_minus.connect(I1.port_minus)
    #I1.port_plus.connection_node.name = 'a'
    #I1.port_minus.connection_node.name = 'b'

    result_keys = (('component voltage', L1),
                   ('component voltage', RL),
                   ('component voltage', R1),
                   ('component voltage', R2))

    time_step = 0.0001
    duration = 0.2

    return R1.port_plus, result_keys, time_step, duration


def example_circuit_rlc1():
    """
        --------+R1 ------
        |+               |+
        E1               L1
        |                |
        ------ C1+--------

    """
    R1 = Resistor('R1', 0.5)
    L1 = Inductor('L1', 1)
    C1 = Capacitor('C1', 1)
    E1 = VSource('E1', 10)

    R1.port_plus.connect(E1.port_plus)
    R1.port_minus.connect(L1.port_plus)
    L1.port_minus.connect(C1.port_plus)
    C1.port_minus.connect(E1.port_minus)

    result_keys = (('component voltage', R1),
                   ('component voltage', L1),
                   ('component voltage', C1),
                   ('component current', R1),
                   )

    time_step = 0.01
    duration = 30

    return R1.port_plus, result_keys, time_step, duration


def example_circuit_rlc2():
    """
        -------------------------
        |+      |+      |+      |+
        I1      R1      L1     C1
        |       |       |       |
        -------------------------

    """
    R1 = Resistor('R1', 0.3)
    L1 = Inductor('L1', 1)
    C1 = Capacitor('C1', 1)
    I1 = ISource('I1', 1)

    R1.port_plus.connect(I1.port_plus)
    L1.port_plus.connect(I1.port_plus)
    C1.port_plus.connect(I1.port_plus)
    R1.port_minus.connect(I1.port_minus)
    L1.port_minus.connect(I1.port_minus)
    C1.port_minus.connect(I1.port_minus)

    result_keys = (('component current', R1),
                   ('component current', L1),
                   ('component current', C1),
                   ('component current', I1),
                   ('component voltage', I1),
                   )

    time_step = 0.001
    duration = 17

    return R1.port_plus, result_keys, time_step, duration


def example_circuit_switch():
    """
        -------+ R1 --------------
        |+               |+      |+
        E1               L1      RL
        |         +      |       |
        -----o S1 o---------------

    """
    R1 = Resistor('R1', 3)
    L1 = Inductor('L1', 3)
    RL = Resistor('RL', 100)
    S1 = Switch('S1', lambda gd: int(gd.simulation_time) % 2)
    E1 = VSource('E1', 1)

    R1.port_plus.connect(E1.port_plus)
    R1.port_minus.connect(L1.port_plus)
    L1.port_minus.connect(S1.port_plus)
    S1.port_minus.connect(E1.port_minus)

    RL.port_plus.connect(L1.port_plus)
    RL.port_minus.connect(L1.port_minus)

    result_keys = (('component voltage', L1),
                   ('component voltage', E1),
                   )

    time_step = 0.0005
    duration = 3

    return R1.port_plus, result_keys, time_step, duration


def example_circuit_rlc_sinus():
    """
        -------------------------
        |+      |+      |+      |+
       E1~      R1      L1     C1
        |       |       |       |
        -------------------------

    """
    R1 = Resistor('R1', 1)
    L1 = Inductor('L1', 1)
    C1 = Capacitor('C1', 1)
    E1 = VSignalGenerator('E1', Shape.Sinus(1, 1 / 2 / math.pi))

    R1.port_plus.connect(E1.port_plus)
    L1.port_plus.connect(E1.port_plus)
    C1.port_plus.connect(E1.port_plus)
    R1.port_minus.connect(E1.port_minus)
    L1.port_minus.connect(E1.port_minus)
    C1.port_minus.connect(E1.port_minus)

    result_keys = (('component current', R1),
                   ('component current', L1),
                   ('component current', C1),
                   ('component current', E1),
                   ('component voltage', E1),
                   )

    time_step = 0.005
    duration = 10

    return R1.port_plus, result_keys, time_step, duration


def example_circuit_diode():
    """
        ------+ R1-----+ D1----+ D2--------
        |+                                |
       E1~                                |
        |                                 |
        -----------------------------------

    """
    R1 = Resistor('R1', 1)
    D1 = IdealDiode('D1', 0.7, 0.008)
    D2 = IdealDiode('D2', 0.7, 0.008)
    E1 = VSignalGenerator('E1', Shape.Sinus(2, 2))

    R1.port_plus.connect(E1.port_plus)
    R1.port_minus.connect(D1.port_plus)
    D1.port_minus.connect(D2.port_plus)
    D2.port_minus.connect(E1.port_minus)

    result_keys = (('component current', R1),
                   ('component voltage', D1),)

    time_step = 0.0005
    duration = 1

    return R1.port_plus, result_keys, time_step, duration


def example_envelope_demodulator():
    """
        o--------+ D1------------------------o
                          |+        |+
     AM input             C1        R1     demodulated output
                          |         |
        o------------------------------------o

    """
    def am_signal(carrier_f, carrier_a, signal_f, signal_a):
        def get_value(t):
            carrier = carrier_a * math.sin(t * carrier_f / 2 / math.pi)
            signal = signal_a * math.sin(t * signal_f / 2 / math.pi)
            return carrier * (signal + carrier_a)
        return get_value

    am = am_signal(50e3, 5, 1e3, 3)

    V1 = VSignalGenerator('AM', lambda gd: am(gd.simulation_time))
    D1 = IdealDiode('D1', 0.7, 0.008)
    C1 = Capacitor('C1', 80e-6)
    R1 = Resistor('demodulated', 100)

    V1.port_plus.connect(D1.port_plus)
    D1.port_minus.connect(R1.port_plus, C1.port_plus)
    V1.port_minus.connect(R1.port_minus, C1.port_minus)

    result_keys = (('component voltage', V1),
                   ('component voltage', R1),)

    time_step = 0.000001
    duration = 0.08

    return R1.port_plus, result_keys, time_step, duration


def run_example(SolverImpl, example):
    entry_point, result_keys, time_step, duration = example()

    s = SolverImpl(entry_point)

    t0 = time.time()
    print(
        '{}: {}'.format(SolverImpl.__name__, example.__name__).center(100, ':'))
    results = s.calculate(result_keys, time_step=time_step, duration=duration)
    t1 = time.time()
    tdiff = t1 - t0

    x = results.get_input_array()
    print('Solver timing for {}:{}:\n    {} seconds ({} per step)'
          .format(example.__name__, SolverImpl.__name__,
                  tdiff, tdiff / len(x)))

    data = [results.get_results(*result_key) for result_key in result_keys]
    names = ['{} {}'.format(t, o.name) for t, o in result_keys]

    return _multiplot(plt, x, zip(names, data), False)

    #plt.xlabel('time (s)')
    # plt.show()


def _multiplot(plt, x, ys, in_the_same_graph=True):
    if in_the_same_graph:
        for _yname, y in ys:
            fig, ax1 = plt.subplots()
            ax1.plot(x, y)
        return fig
    else:
        ys = list(ys)
        rows = int(math.ceil(math.sqrt(len(ys))))
        columns = int(math.ceil(len(ys) / rows))
        fig, axes = plt.subplots(rows, columns, squeeze=False)
        fig.text(0, 0, str(example))
        axes = [c for r in axes for c in r]
        for ax, (yname, y) in zip(axes, ys):
            ax.plot(x, y)
            ax.set_xlabel('time (s)')
            ax.set_ylabel(yname)
        return fig

examples = [
    example_circuit1,
    example_circuit_rlc1,
    example_circuit_rlc2,
    example_circuit_switch,
    example_circuit_rlc_sinus,
    example_circuit_diode,
    example_envelope_demodulator
]
for example in examples:
    for SolverImpl in (Solver, ):
        fig = run_example(SolverImpl, example)
        fig.show()
fig.waitforbuttonpress()
