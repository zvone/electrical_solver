import pytest

from components import ISource, Resistor, VSource, Switch
from solver.topology import circuit_sanity_check, SanityCheckFailed


def test_sanity_only_isource():
    """
        --- I1+----------
        |               |
        -----------------

        OK
    """
    I1 = ISource('I1', 1)
    I1.port_plus.connect(I1.port_minus)

    circuit_sanity_check(I1.port_plus)


def test_sanity_two_isources():
    """
        --- I1+----------
        |               |
        --- I2+----------

        Not OK: conflicting current values
        Note: even if they where by chance equal, voltage would be unsolvable
    """
    I1 = ISource('I1', 1)
    I2 = ISource('I2', 1)
    I1.port_plus.connect(I2.port_plus)
    I1.port_minus.connect(I2.port_minus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(I1.port_plus)


def test_sanity_only_isource_r():
    """
        --- I1+----------
        |               |
        -- R1+-----------

        OK
    """
    I1 = ISource('I1', 1)
    R1 = Resistor('R1', 1)
    I1.port_plus.connect(R1.port_plus)
    I1.port_minus.connect(R1.port_minus)

    circuit_sanity_check(I1.port_plus)


def test_sanity_1_isource_2r():
    """
        --- I1+----------
        |               |
        -- R1+-----------
        |               |
        -- R2+-----------

        OK
    """
    I1 = ISource('I1', 1)
    R1 = Resistor('R1', 1)
    R2 = Resistor('R2', 1)
    I1.port_plus.connect(R1.port_plus, R2.port_plus)
    I1.port_minus.connect(R1.port_minus, R2.port_minus)

    circuit_sanity_check(I1.port_plus)


def test_sanity_parallel_double_isource_r():
    """
        --- I1+----------
        |               |
        --- I2+----------
        |               |
        -- R1+-----------

        OK
    """
    I1 = ISource('I1', 1)
    I2 = ISource('I2', 1)
    R1 = Resistor('R1', 1)
    I1.port_plus.connect(R1.port_plus, I2.port_plus)
    I1.port_minus.connect(R1.port_minus, I2.port_minus)

    circuit_sanity_check(I1.port_plus)


def test_sanity_double_serial_isource_r():
    """
        --- I1+--- I2+---
        |               |
        ------ R1+-------

        Not OK: conflicting currents
        Note: even if they where by chance equal, voltage would be unsolvable
    """
    I1 = ISource('I1', 1)
    I2 = ISource('I2', 2)
    R1 = Resistor('R1', 1)
    I1.port_plus.connect(I2.port_minus)

    I1.port_minus.connect(R1.port_minus)
    I2.port_plus.connect(R1.port_plus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(R1.port_plus)


def test_sanity_double_serial_isource_double_r():
    """
        --- I1+--- I2+---
        |               |
        ------ R1+-------
        |               |
        ------ R2+-------

        Not OK: conflicting currents
        Note: even if they where by chance equal, voltage would be unsolvable
    """
    I1 = ISource('I1', 1)
    I2 = ISource('I2', 2)
    R1 = Resistor('R1', 1)
    R2 = Resistor('R2', 1)
    I1.port_plus.connect(I2.port_minus)

    I1.port_minus.connect(R1.port_minus, R2.port_minus)
    I2.port_plus.connect(R1.port_plus, R2.port_plus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(R1.port_plus)


def test_sanity_double_real_isource_double_r():
    """
        --- I1+---- I2+---
        |       | |      |
        -- RI1+-  - RI2+--
        |                |
        ------ R1+--------
        |                |
        ------ R2+--------

        OK: RI1 and RI2 compensate for current differences
    """
    I1 = ISource('I1', 1)
    I2 = ISource('I2', 2)
    RI1 = Resistor('RI1', 1)
    RI2 = Resistor('RI2', 1)

    I1.port_minus.connect(RI1.port_minus)
    I1.port_plus.connect(RI1.port_plus)
    I2.port_minus.connect(RI2.port_minus)
    I2.port_plus.connect(RI2.port_plus)

    R1 = Resistor('R1', 1)
    R2 = Resistor('R2', 1)
    I1.port_plus.connect(I2.port_minus)

    I1.port_minus.connect(R1.port_minus, R2.port_minus)
    I2.port_plus.connect(R1.port_plus, R2.port_plus)

    circuit_sanity_check(R1.port_plus)


def test_sanity_only_vsource():
    """
        --- V1+----------
        |               |
        -----------------

        Not OK: V1 voltage conflicts to required zero voltage
        Note: Voltage woud be ok if V=0, but current would still be unsolvable
    """
    V1 = VSource('V1', 1)
    V1.port_plus.connect(V1.port_minus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(V1.port_plus)


def test_sanity_vsource_r():
    """
        --- V1+----------
        |               |
        --- R1+----------

        OK
    """
    V1 = VSource('V1', 1)
    R1 = Resistor('R1', 1)
    V1.port_plus.connect(R1.port_plus)
    V1.port_minus.connect(R1.port_minus)

    circuit_sanity_check(V1.port_plus)


def test_sanity_1_vsource_2r():
    """
        --- V1+----------
        |               |
        -- R1+-----------
        |               |
        -- R2+-----------

        OK
    """
    V1 = VSource('V1', 1)
    R1 = Resistor('R1', 1)
    R2 = Resistor('R2', 1)
    V1.port_plus.connect(R1.port_plus, R2.port_plus)
    V1.port_minus.connect(R1.port_minus, R2.port_minus)

    circuit_sanity_check(V1.port_plus)


def test_sanity_double_parallel_vsource_r():
    """
        --- V1+----------
        |               |
        --- V2+----------
        |               |
        --- R1+----------

        Not OK: V1 voltage conflicts with V2 voltage
        Note: even if they where by chance equal, currents would be unsolvable
    """
    V1 = VSource('V1', 1)
    V2 = VSource('V2', 1)
    R1 = Resistor('R1', 1)
    V1.port_plus.connect(R1.port_plus, V2.port_plus)
    V1.port_minus.connect(R1.port_minus, V2.port_minus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(V1.port_plus)


def test_sanity_double_serial_vsource_r():
    """
        --- V1+--- V2+---
        |               |
        --- R1+----------

        OK
    """
    V1 = VSource('V1', 1)
    V2 = VSource('V2', 1)
    R1 = Resistor('R1', 1)
    V1.port_plus.connect(V2.port_minus)
    V1.port_minus.connect(R1.port_minus)
    V2.port_plus.connect(R1.port_plus)

    circuit_sanity_check(V1.port_plus)


def test_sanity_vsource_self_loop():
    """
        --- V1+--- V2+---
        |       |       |
        --- R1+----------

        Not OK: V2 conflicts with required voltage 0
    """
    V1 = VSource('V1', 1)
    V2 = VSource('V2', 1)
    R1 = Resistor('R1', 1)
    V1.port_plus.connect(V2.port_minus)
    V1.port_minus.connect(R1.port_minus)
    V2.port_plus.connect(R1.port_plus)
    V1.port_plus.connect(R1.port_plus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(V1.port_plus)


def test_sanity_double_parallel_real_vsource_r():
    """
        --- V1+-- RV1+---
        |               |
        --- V2+-- RV2+---
        |               |
        --- R1+----------

        OK
    """
    V1 = VSource('V1', 1)
    V2 = VSource('V2', 1)
    RV1 = Resistor('RV1', 1)
    RV2 = Resistor('RV2', 1)
    R1 = Resistor('R1', 1)
    V1.port_plus.connect(RV1.port_minus)
    V2.port_plus.connect(RV2.port_minus)

    RV1.port_plus.connect(R1.port_plus, RV2.port_plus)
    V1.port_minus.connect(R1.port_minus, V2.port_minus)

    circuit_sanity_check(V1.port_plus)


def test_sanity_dangling_resistor():
    """
        --- V1+----------
        |               |
        --- R1+------------- R2+-----o

        OK
    """
    V1 = VSource('V1', 1)
    R1 = Resistor('R1', 1)
    R2 = Resistor('R2', 1)
    V1.port_plus.connect(R1.port_plus)
    V1.port_minus.connect(R1.port_minus)
    R1.port_plus.connect(R2.port_minus)

    circuit_sanity_check(R1.port_plus)


def test_sanity_dangling_switch():
    """
        --- V1+----------
        |               |
        --- R1+-------------o S1 o-----o

        Not OK: unknown switch voltage when OFF
    """
    V1 = VSource('V1', 1)
    R1 = Resistor('R1', 1)
    S1 = Switch('S1', 1)
    V1.port_plus.connect(R1.port_plus)
    V1.port_minus.connect(R1.port_minus)
    R1.port_plus.connect(S1.port_minus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(R1.port_plus)


def test_sanity_dangling_switch_resistor():
    """
        --- V1+----------
        |               |
        --- R1+-------------o S1 o+-- R2+--o

        Not OK: unknown switch voltage when OFF
    """
    V1 = VSource('V1', 1)
    R1 = Resistor('R1', 1)
    S1 = Switch('S1', 1)
    R2 = Resistor('R2', 1)
    V1.port_plus.connect(R1.port_plus)
    V1.port_minus.connect(R1.port_minus)
    R1.port_plus.connect(S1.port_minus)
    S1.port_plus.connect(R2.port_minus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(R1.port_plus)


def test_sanity_dangling_isource():
    """
        --- V1+----------
        |               |
        --- R1+------------- I1+-----o

        Not OK: impossible current through dangling branch
    """
    V1 = VSource('V1', 1)
    R1 = Resistor('R1', 1)
    I1 = ISource('I1', 1)
    V1.port_plus.connect(R1.port_plus)
    V1.port_minus.connect(R1.port_minus)
    R1.port_plus.connect(I1.port_minus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(R1.port_plus)


def test_sanity_dangling_isource_r():
    """
        --- V1+----------
        |               |
        --- R1+------------- I1+--- R2+--o

        Not OK: impossible current through dangling branch
    """
    V1 = VSource('V1', 1)
    R1 = Resistor('R1', 1)
    I1 = ISource('I1', 1)
    R2 = Resistor('R2', 1)
    V1.port_plus.connect(R1.port_plus)
    V1.port_minus.connect(R1.port_minus)
    R1.port_plus.connect(I1.port_minus)
    I1.port_plus.connect(R2.port_minus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(R1.port_plus)


def test_sanity_dangling_vsource():
    """
        --- V1+----------
        |               |
        --- R1+------------- V2+-----o

        OK
    """
    V1 = VSource('V1', 1)
    R1 = Resistor('R1', 1)
    V2 = Resistor('V2', 1)
    V1.port_plus.connect(R1.port_plus)
    V1.port_minus.connect(R1.port_minus)
    R1.port_plus.connect(V2.port_minus)

    circuit_sanity_check(R1.port_plus)


def test_sanity_vsource_switch():
    """
        --- V1+----------
        |               |
        ----o S1 o+------

        Not OK: impossible voltage if S1 is ON 
    """
    V1 = VSource('V1', 1)
    S1 = Switch('S1', 1)
    V1.port_plus.connect(S1.port_plus)
    V1.port_minus.connect(S1.port_minus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(S1.port_plus)


def test_sanity_vsource_switch_r():
    """
        --- V1+---+R1 ---
        |               |
        ----o S1 o+------

        OK
    """
    V1 = VSource('V1', 1)
    R1 = Resistor('R1', 1)
    S1 = Switch('S1', 1)
    V1.port_plus.connect(R1.port_plus)
    V1.port_minus.connect(S1.port_minus)
    S1.port_plus.connect(R1.port_minus)

    circuit_sanity_check(S1.port_plus)


def test_sanity_vsource_double_switch_r():
    """
        --- V1+---+R1 ---
        |               |
        ----o S1 o+------
        |               |
        ----o S2 o+------

        Not OK: Currents through switches not solvable
    """
    V1 = VSource('V1', 1)
    R1 = Resistor('R1', 1)
    S1 = Switch('S1', 1)
    S2 = Switch('S2', 1)
    V1.port_plus.connect(R1.port_plus)
    V1.port_minus.connect(S1.port_minus, S2.port_minus)
    R1.port_minus.connect(S1.port_plus, S2.port_plus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(S1.port_plus)


def test_sanity_vsource_switch_parallel_r():
    """
        --- V1+----------
        |               |
        ----o S1 o+------
        |               |
        --- R1+----------

        Not OK: impossible voltage if S1 is ON 
    """
    V1 = VSource('V1', 1)
    S1 = Switch('S1', 1)
    R1 = Resistor('R1', 1)
    V1.port_plus.connect(S1.port_plus, R1.port_plus)
    V1.port_minus.connect(S1.port_minus, R1.port_minus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(S1.port_plus)


def test_sanity_isource_switch():
    """
        --- I1+----------
        |               |
        ----o S1 o+------

        Not OK: impossible current if S1 is OFF 
    """
    I1 = ISource('I1', 1)
    S1 = Switch('S1', 1)
    I1.port_plus.connect(S1.port_plus)
    I1.port_minus.connect(S1.port_minus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(S1.port_plus)


def test_sanity_isource_switch_r():
    """
        --- I1+---+R1 ---
        |               |
        ----o S1 o+------

        Not OK: impossible current if S1 is OFF 
    """
    I1 = ISource('I1', 1)
    R1 = Resistor('R1', 1)
    S1 = Switch('S1', 1)
    I1.port_plus.connect(R1.port_plus)
    I1.port_minus.connect(S1.port_minus)
    S1.port_plus.connect(R1.port_minus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(S1.port_plus)


def test_sanity_isource_switch_parallel_r():
    """
        --- I1+----------
        |               |
        ----o S1 o+------
        |               |
        --- R1+----------

        OK
    """
    I1 = ISource('I1', 1)
    S1 = Switch('S1', 1)
    R1 = Resistor('R1', 1)
    I1.port_plus.connect(S1.port_plus, R1.port_plus)
    I1.port_minus.connect(S1.port_minus, R1.port_minus)

    circuit_sanity_check(S1.port_plus)


def test_sanity_vsource_triangle():
    """
        -------------- R1+-----------------
        |                                 |
        -------------- V1+-----------------
        |                                 |
        --------- V2+------------ V3+------
        |                |                |
        ------- R2+---------------R3+------

        Not OK: conflict with requirement V1 = V2 + V3
    """
    V1 = VSource('V1', 1)
    V2 = VSource('V2', 1)
    V3 = VSource('V3', 1)
    R1 = Resistor('R1', 1)
    R2 = Resistor('R2', 1)
    R3 = Resistor('R3', 1)
    R1.port_minus.connect(V1.port_minus, V2.port_minus, R2.port_minus)
    R1.port_plus.connect(V1.port_plus, V3.port_plus, R3.port_plus)
    V2.port_plus.connect(V3.port_minus, R2.port_plus, R3.port_minus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(V1.port_plus)


def test_sanity_isource_star():
    """
            -------------- R3+-----------------
            |                                 |
    -- R2+---- I1+-- RI1+------ I2+---- RI2+---
    |                        |                |
    ---------- I3+-- RI3+-----
            |                                 |
            ---------------- R1+---------------

        Not OK: conflict with requirement I1 - I2 + I3 = 0
    """
    I1 = ISource('I1', 1)
    I2 = ISource('I2', 1)
    I3 = ISource('I3', 1)
    R1 = Resistor('R1', 1)
    R2 = Resistor('R2', 1)
    R3 = Resistor('R3', 1)
    RI1 = Resistor('RI1', 1)
    RI2 = Resistor('RI2', 1)
    RI3 = Resistor('RI3', 1)

    I1.port_plus.connect(RI1.port_minus)
    I2.port_plus.connect(RI2.port_minus)
    I3.port_plus.connect(RI3.port_minus)

    I1.port_minus.connect(R2.port_plus, R3.port_minus)
    RI2.port_plus.connect(R3.port_plus, R1.port_plus)
    I3.port_minus.connect(R2.port_minus, R1.port_minus)

    I2.port_minus.connect(RI1.port_plus, RI3.port_plus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(I1.port_plus)


def test_sanity_current_between_networks():
    """
        --- R1+-------- I1+--------- R5+--
        |       |                |       |
        R2      R3               R6      R7
        +       +                +       +
        |       |                |       |
        --- R4+-------- I2+-------- R8+---

        Not OK: conflict with requirement I1 = -I2
    """
    I1 = ISource('I1', 1)
    I2 = ISource('I2', 1)
    R1 = Resistor('R1', 1)
    R2 = Resistor('R2', 1)
    R3 = Resistor('R3', 1)
    R4 = Resistor('R4', 1)
    R5 = Resistor('R5', 1)
    R6 = Resistor('R6', 1)
    R7 = Resistor('R7', 1)
    R8 = Resistor('R8', 1)
    I1.port_plus.connect(R5.port_minus, R6.port_minus)
    R5.port_plus.connect(R7.port_minus)
    R7.port_plus.connect(R8.port_plus)
    R8.port_minus.connect(R6.port_plus, I2.port_plus)
    I2.port_minus.connect(R4.port_plus, R3.port_plus)
    R4.port_minus.connect(R2.port_plus)
    R2.port_minus.connect(R1.port_minus)
    R1.port_plus.connect(R3.port_minus, I1.port_minus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(I1.port_plus)


def test_sanity_current_switch_between_networks():
    """
        --- R1+-------- I1+--------- R5+--
        |       |                |       |
        R2      R3               R6      R7
        +       +                +       +
        |       |                |       |
        --- R4+------o S1 o+------- R8+---

        Not OK: conflict with requirement I1 = -I2
    """
    I1 = ISource('I1', 1)
    S1 = Switch('S1', 1)
    R1 = Resistor('R1', 1)
    R2 = Resistor('R2', 1)
    R3 = Resistor('R3', 1)
    R4 = Resistor('R4', 1)
    R5 = Resistor('R5', 1)
    R6 = Resistor('R6', 1)
    R7 = Resistor('R7', 1)
    R8 = Resistor('R8', 1)
    I1.port_plus.connect(R5.port_minus, R6.port_minus)
    R5.port_plus.connect(R7.port_minus)
    R7.port_plus.connect(R8.port_plus)
    R8.port_minus.connect(R6.port_plus, S1.port_plus)
    S1.port_minus.connect(R4.port_plus, R3.port_plus)
    R4.port_minus.connect(R2.port_plus)
    R2.port_minus.connect(R1.port_minus)
    R1.port_plus.connect(R3.port_minus, I1.port_minus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(I1.port_plus)


def test_sanity_completely_separated_networks():
    """
        --- R1+--                --- R5+--
        |       |                |       |
        R2      R3               R6      R7
        +       +                +       +
        |       |                |       |
        --- R4+------o S1 o+------- R8+---

        Not OK: unknown voltage on S1 when OFF
    """
    S1 = Switch('S1', 1)
    R1 = Resistor('R1', 1)
    R2 = Resistor('R2', 1)
    R3 = Resistor('R3', 1)
    R4 = Resistor('R4', 1)
    R5 = Resistor('R5', 1)
    R6 = Resistor('R6', 1)
    R7 = Resistor('R7', 1)
    R8 = Resistor('R8', 1)
    R5.port_minus.connect(R6.port_minus)
    R5.port_plus.connect(R7.port_minus)
    R7.port_plus.connect(R8.port_plus)
    R8.port_minus.connect(R6.port_plus, S1.port_plus)
    S1.port_minus.connect(R4.port_plus, R3.port_plus)
    R4.port_minus.connect(R2.port_plus)
    R2.port_minus.connect(R1.port_minus)
    R1.port_plus.connect(R3.port_minus)

    with pytest.raises(SanityCheckFailed):
        circuit_sanity_check(R1.port_plus)
