import pytest

import numpy as np

from components import VSource, Resistor, Capacitor, Switch, ISignalGenerator,\
    Inductor
from solver.solver import Solver

from solver.solver import Solver as NumericSolver
@pytest.fixture(params=[NumericSolver, ])
def Solver(request):
    return request.param


def test_capacitor_charging(Solver):
    """
        --- V1+---+R1 ---
        |               |
        ------- C1+------

    """
    V1 = VSource('V1', 10)
    R1 = Resistor('R1', 10)
    C1 = Capacitor('R2', 1)
    V1.port_plus.connect(R1.port_plus)
    R1.port_minus.connect(C1.port_plus)
    C1.port_minus.connect(V1.port_minus)

    s = Solver(R1.port_minus)
    results = s.calculate([('component voltage', C1)], 0.001, 20)
    times = results.get_input_array()
    expected_voltages = V1.v * (- np.expm1(-times / (R1.r * C1.c)))
    calculated_voltages = results.get_results('component voltage', C1)
    assert expected_voltages == pytest.approx(calculated_voltages, rel=1e-4)


def test_multi_capacitor_charging(Solver):
    """
        --- V1+---+C1 ---
        |               |
        ------- C2+------

    """
    V1 = VSource('V1', 120)
    C1 = Capacitor('C1', 20e-9)
    C2 = Capacitor('C2', 30e-9)
    V1.port_plus.connect(C1.port_plus)
    C1.port_minus.connect(C2.port_plus)
    C2.port_minus.connect(V1.port_minus)

    results = Solver(C1.port_minus).calculate_single_step(('component voltage', C1),
                                                          ('component voltage', C2))
    assert results.get_result('component voltage', C1) == pytest.approx(72)
    assert results.get_result('component voltage', C2) == pytest.approx(48)

    C2.initial_voltage = 20

    results = Solver(C1.port_minus).calculate_single_step(('component voltage', C1),
                                                          ('component voltage', C2))
    assert results.get_result('component voltage', C1) == pytest.approx(60)
    assert results.get_result('component voltage', C2) == pytest.approx(60)

    C2.initial_voltage = -20

    results = Solver(C1.port_minus).calculate_single_step(('component voltage', C1),
                                                          ('component voltage', C2))
    assert results.get_result('component voltage', C1) == pytest.approx(84)
    assert results.get_result('component voltage', C2) == pytest.approx(36)


def test_capacitor_charge_discharge(Solver):
    """
        --- V1+---+RV ---
        |               |
        -----o S1 o+-----
        |               |
        --- C1+--- RC+---

    """
    S1 = Switch('S1', lambda gd: gd.simulation_time > 30e-3)
    C1 = Capacitor('C1', 5e-6)
    RV = Resistor('RV', 600)
    RC = Resistor('RC', 2.4e3)
    V1 = VSource('V1', 12)

    V1.port_plus.connect(RV.port_plus)
    C1.port_plus.connect(RC.port_minus)
    V1.port_minus.connect(S1.port_minus, C1.port_minus)
    RV.port_minus.connect(S1.port_plus, RC.port_plus)

    s = Solver(V1.port_minus)
    results = s.calculate([('component voltage', C1)], 0.001e-3, 42e-3)
    assert results.get_results('component voltage', C1)[-1] \
        == pytest.approx(3.817, rel=1e-4)
