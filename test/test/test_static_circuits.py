import pytest

from components import ISource, Resistor, VSource, Switch, FixedPotentialPoint,\
    Ground
from solver.solver import Solver as NumericSolver

@pytest.fixture(params=[NumericSolver,])
def Solver(request):
    return request.param


def test_i_r(Solver):
    """
        --- I1+----------
        |               |
        -- R1+-----------
    """
    I1 = ISource('I1', 12)
    R1 = Resistor('R1', 0.5)
    I1.port_plus.connect(R1.port_plus)
    I1.port_minus.connect(R1.port_minus)

    s = Solver(R1.port_minus)

    results = s.calculate_single_step(  # ('component voltage', R1),
        ('component voltage', I1))
    #assert results.get_result('component voltage', R1) == 6
    assert results.get_result('component voltage', I1) == 6


def test_v_r(Solver):
    """
        --- V1+----------
        |               |
        -- R1+-----------
    """
    V1 = VSource('V1', 12)
    R1 = Resistor('R1', 0.5)
    V1.port_plus.connect(R1.port_plus)
    V1.port_minus.connect(R1.port_minus)

    s = Solver(R1.port_minus)
    results = s.calculate_single_step(('component current', R1),
                                      ('component voltage', V1))
    assert results.get_result('component current', R1) == 24
    assert results.get_result('component voltage', V1) == 12


def test_v_r2(Solver):
    """
        --- V1+----------
        |               |
        -- R1+---- R2+---

    """
    V1 = VSource('V1', 12)
    R1 = Resistor('R1', 0.5)
    R2 = Resistor('R2', 1.5)
    V1.port_plus.connect(R2.port_plus)
    R2.port_minus.connect(R1.port_plus)
    V1.port_minus.connect(R1.port_minus)

    s = Solver(R1.port_minus)
    assert s.calculate_single_step(('component current', R1)
                                   ).get_result('component current', R1) == 6


def test_network_1_101(Solver):
    """
    -----------------------------------------
    |      |          |             |       |
    |+     |+         |+            |+      |+
    R1     R2         Rx            R3      R4
    |      |          |             |       |
    |      |          |             |       |
    ---------- R5+--------- R6+--------------
    |      |                        |       |
    |+     |+                       |+      |+
    R7     V1                       V2      R8
    |      |                        |       |
    |      |                        |       |
    -----------------------------------------
    """
    V1 = VSource('V1', 200)
    V2 = VSource('V2', 100)
    R1 = Resistor('R1', 60)
    R2 = Resistor('R2', 60)
    R3 = Resistor('R3', 30)
    R4 = Resistor('R4', 30)
    R5 = Resistor('R5', 60)
    R6 = Resistor('R6', 30)
    R7 = Resistor('R7', 15)
    R8 = Resistor('R8', 15)
    Rx = Resistor('Rx', 0)
    R1.port_plus.connect(R2.port_plus, R3.port_plus,
                         R4.port_plus, Rx.port_plus)
    R1.port_minus.connect(R2.port_minus, R5.port_minus,
                          V1.port_plus, R7.port_plus)
    R4.port_minus.connect(R3.port_minus, R6.port_plus,
                          V2.port_plus, R8.port_plus)
    R5.port_plus.connect(R6.port_minus, Rx.port_minus)
    R7.port_minus.connect(V1.port_minus, V2.port_minus, R8.port_minus)

    s = Solver(R1.port_minus)
    result1 = s.calculate_single_step(('component voltage', R1),
                                      ('component voltage', R4),
                                      ('component voltage', Rx))
    assert result1.get_result(
        'component voltage', R1) == pytest.approx(-200 / 3)
    assert result1.get_result(
        'component voltage', R4) == pytest.approx(100 / 3)
    assert result1.get_result('component voltage', Rx) == pytest.approx(0)


def test_fixed_potential_circuit(Solver):
    """
                         ^ 5V
                         |
        ------- R1+-------
        |
       ---
       GND
    """
    V5 = FixedPotentialPoint('5V', 5)
    R1 = Resistor('R1', 2)
    GND = Ground('GND')

    V5.port.connect(R1.port_plus)
    GND.port.connect(R1.port_minus)

    s = Solver(R1.port_minus)
    result = s.calculate_single_step(('component current', R1))
    assert result.get_result('component current', R1) == 2.5


def test_double_fixed_potential_circuit(Solver):
    """
                         ^ 5V   ^ 5V
                         |      |
        ------- R1+--------------
        |
       ---
       GND
    """
    V5 = FixedPotentialPoint('5V', 5)
    V5b = FixedPotentialPoint('5Vb', 5)
    R1 = Resistor('R1', 2)
    GND = Ground('GND')

    V5.port.connect(R1.port_plus)
    V5b.port.connect(R1.port_plus)
    GND.port.connect(R1.port_minus)
    s = Solver(R1.port_minus)
    result = s.calculate_single_step(('component current', R1))
    assert result.get_result('component current', R1) == 2.5


def test_fixed_potential_with_source(Solver):
    """
      a o----- R1+---- V1+-----o b
    """
    a = FixedPotentialPoint('a', 10)
    R1 = Resistor('R1', 5)
    V1 = VSource('V1', 15)
    b = FixedPotentialPoint('b', 30)

    a.port.connect(R1.port_minus)
    R1.port_plus.connect(V1.port_minus)
    V1.port_plus.connect(b.port)

    s = Solver(R1.port_minus)
    result = s.calculate_single_step(('component current', R1))
    assert result.get_result('component current', R1) == 1


def test_wheatsone(Solver):
    """
        -----+R1 --------+R2 -----
        |           |            |
        |       Voltmeter        |
        |           |            |
        -----+R3 --------+R4 -----
        |                        |
        -----------+V1 -----------
    """
    R1 = Resistor('R1', 3)
    R2 = Resistor('R2', 6)
    R3 = Resistor('R3', 6)
    R4 = Resistor('R4', 3)
    Voltmeter = Resistor('Voltmeter', 10**20)
    V1 = VSource('V1', 18)

    R1.port_plus.connect(R3.port_plus, V1.port_plus)
    R2.port_minus.connect(R4.port_minus, V1.port_minus)

    R1.port_minus.connect(R2.port_plus, Voltmeter.port_plus)
    R3.port_minus.connect(R4.port_plus, Voltmeter.port_minus)

    s = Solver(R1.port_minus)
    result = s.calculate_single_step(('component voltage', Voltmeter))
    assert result.get_result(
        'component voltage', Voltmeter) == pytest.approx(6)


def test_another_network(Solver):
    r"""
    -------------------------------------------------
    |                                               |
    --------------------+ R4--------------------   Voltmeter
    |+     |      | \            |      |      |+   |
    R1     |      |  \           |      |      R8   |
    |      |+     |+  \          |+     |+     |-----
    |+     I1     R3   -+ R5--   R7     I2     |+
    R2     |      |           \  |      |      R9
    |      |      |            \ |      |      |
    ------------------- R6+---------------------
    """
    R1 = Resistor('R1', 1000 / 12)
    R2 = Resistor('R2', 1000 / 6)
    R3 = Resistor('R3', 1000 / 5)
    R4 = Resistor('R4', 1000 / 4)
    R5 = Resistor('R5', 1000000 / 125)
    R6 = Resistor('R6', 1000 / 2.2)
    R7 = Resistor('R7', 1000 / 1)
    R8 = Resistor('R8', 1000 / 4)
    R9 = Resistor('R9', 1000 / 12)
    I1 = ISource('I1', 32 / 1000)
    I2 = ISource('I2', 112 / 1000)
    R1.port_minus.connect(R2.port_plus)
    R1.port_plus.connect(
        I1.port_plus, R3.port_plus, R4.port_plus, R5.port_plus)
    R2.port_minus.connect(I1.port_minus, R3.port_minus, R6.port_plus)
    R4.port_minus.connect(R7.port_plus, I2.port_plus, R8.port_plus)
    R5.port_minus.connect(
        R6.port_minus, R7.port_minus, I2.port_minus, R9.port_minus)
    R8.port_minus.connect(R9.port_plus)

    Voltmeter = Resistor('Voltmeter', 1e20)
    Voltmeter.port_plus.connect(R1.port_minus)
    Voltmeter.port_minus.connect(R8.port_minus)

    s = Solver(R1.port_minus)
    result = s.calculate_single_step(('component voltage', Voltmeter))
    assert result.get_result(
        'component voltage', Voltmeter) == pytest.approx(8.5)
